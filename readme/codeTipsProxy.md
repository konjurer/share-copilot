# 代码补全 代理说明

 **-以windows为例** 

## **vscode：**

#### 1.修改文件：**

`%userprofile%\.vscode\extensions\github.copilot-xxxx\dist\extension.js`


**1.1.extension.js全文搜索替换下面两个接口，替换为自己的url** 


```js
"https://copilot-proxy.githubusercontent.com" 
替换为"https://127.0.0.1" (你的url)
这个是代码提示接口
---------------------------------------------------------------
"https://copilot-telemetry.githubusercontent.com/telemetry"
替换为"https://127.0.0.1/telemetry" (你的url/telemetry)
这个应该是copilot把你代码上传拿去深度学习的,直接改成null好像也不影响使用
```

**1.2.也可以使用[脚本](https://gitee.com/chuangxxt/share-copilot/releases/download/v2.0.4/vscode%E4%B8%80%E9%94%AE%E9%85%8D%E7%BD%AE%E8%84%9A%E6%9C%AC.bat)一键修改，需要将extension.js原来增加的内容删掉，再配置脚本一键修改，脚本会自动备份原来的文件**


```bash
@echo off
setlocal

:: 将127.0.0.1改为自己的Api url  GITHUB_TOKEN改为自己代理服务器中间件验证内容
set "GITHUB_TOKEN=123456"
set "GITHUB_API_URL=http://127.0.0.1"
set "completionsUrl=http://127.0.0.1"
set "telemetryUrl=http://127.0.0.1/telemetry"

set "CODESPACES=true"
set "GITHUB_SERVER_URL=https://github.com"

...略...
```

#### 2.测试

全部接口代理后是走一个ip了，虽然不知道有没有意义

不过可以搞点有意思的东西，修改一下配置文件

```json
"diyMsg":"//授权已经过期，重新获取-> https://gitee.com/chuangxxt/share-copilot",
"isModMsg":true
```
![输入图片说明](ceshi.gif)



## **jetbrains：**

~~代理服务器是支持的，插件我不会改，等个有缘人~~

~~我修改了插件里面js文件里面的接口，改为自己的，然后重新构建成exe，~~
~~打包后有问题，可以运行，代理联网等都正常~~

~~但是代码提示无法在ide里面显示~~
~~目前找不到原因（不懂node.js）~~
