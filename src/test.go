package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
)

func getGithubTest(c *gin.Context, token string) {
	githubApiCount++
	data1 := `
	{
	    "chat_enabled": false,
	    "code_quote_enabled": false,
	    "code_quote_v2_enabled": false,
	    "copilotignore_enabled": false,
	    "expires_at": 3194360727,
	    "prompt_8k": true,
	    "public_suggestions": "disabled",
	    "refresh_in": 1500,
	    "sku": "free_educational",
	    "telemetry": "disabled",
	    "token": "tid=;exp=1694360727;sku=free_educational;st=dotcom;8kp=1:",
	    "tracking_id": ""
	}
	`
	data2 := `
	{
	    "chat_enabled": false,
	    "code_quote_enabled": false,
	    "code_quote_v2_enabled": false,
	    "copilotignore_enabled": false,
	    "expires_at": 2294360727,
	    "prompt_8k": true,
	    "public_suggestions": "disabled",
	    "refresh_in": 1500,
	    "sku": "free_educational",
	    "telemetry": "disabled",
	    "token": "tid=;exp=1694360727;sku=free_educational;st=dotcom;8kp=1:",
	    "tracking_id": ""
	}
	`
	data3 := `
	{
	    "chat_enabled": false,
	    "code_quote_enabled": false,
	    "code_quote_v2_enabled": false,
	    "copilotignore_enabled": false,
	    "expires_at": 3394360727,
	    "prompt_8k": true,
	    "public_suggestions": "disabled",
	    "refresh_in": 1500,
	    "sku": "free_educational",
	    "telemetry": "disabled",
	    "token": "tid=;exp=1694360727;sku=free_educational;st=dotcom;8kp=1:",
	    "tracking_id": ""
	}
	`
	//响应体map
	var respDataMap = make(map[string]interface{})
	if token == "1" {
		err := json.Unmarshal([]byte(data1), &respDataMap)
		if err != nil {
			// 处理JSON解析错误
			c.JSON(http.StatusInternalServerError, gin.H{"error": "JSON parsing error"})
			return
		}
	} else if token == "2" {
		err := json.Unmarshal([]byte(data2), &respDataMap)
		if err != nil {
			// 处理JSON解析错误
			c.JSON(http.StatusInternalServerError, gin.H{"error": "JSON parsing error"})
			return
		}
	} else {
		err := json.Unmarshal([]byte(data3), &respDataMap)
		if err != nil {
			// 处理JSON解析错误
			c.JSON(http.StatusInternalServerError, gin.H{"error": "JSON parsing error"})
			return
		}
	}

	//token map
	copilotTokenMap[token] = respDataMap
}
