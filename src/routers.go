package main

// Routes 自定义代理服务器路由    附加中间件(域名验证和请求验证)
func Routes() {
	// 定义根路由
	copilotApi := copilotGinEngine.Group("/copilot_internal", DomainMiddleware(configFile.Server.Domain), VerifyRequestMiddleware())

	// 定义代理github token的路由
	copilotApi.GET("/v2/token", getCopilotToken())

	// 定义 completions路由的反向代理
	registerProxyRoute(copilotGinEngine, "/v1/engines/copilot-codex/completions", completionUrl)

	// 定义 telemetry路由的反向代理 ,
	registerProxyRoute(copilotGinEngine, "/telemetry", telemetryUrl)
}
