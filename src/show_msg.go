package main

import (
	"fmt"
	"github.com/fatih/color"
	"strconv"
	"time"
)

// 重置计数
func resetRequestCount() {
	requestCountMutex.Lock()
	defer requestCountMutex.Unlock()
	requestCount = 0
	successCount = 0
}

// 控制台显示信息  无关紧要的内容
func showMsg() {
	fmt.Println(color.WhiteString("-----------------------------------------------------------------------"))
	fmt.Println(color.HiBlueString("     _                                          _ _       _   \n ___| |__   __ _ _ __ ___        ___ ___  _ __ (_) | ___ | |_ \n/ __| '_ \\ / _` | '__/ _ \\_____ / __/ _ \\| '_ \\| | |/ _ \\| __|\n\\__ \\ | | | (_| | | |  __/_____| (_| (_) | |_) | | | (_) | |_ \n|___/_| |_|\\__,_|_|  \\___|      \\___\\___/| .__/|_|_|\\___/ \\__|\n                                         |_|                  \n"))
	fmt.Println(color.WhiteString("[Source Code]") + "https://gitee.com/chuangxxt/share-copilot")
	fmt.Println(color.WhiteString("-----------------------------------------------------------------------"))
	var url = ""
	if configFile.Server.Port == 80 {
		url = "http://" + configFile.Server.Domain
	} else if configFile.Server.Port == 443 {
		url = "https://" + configFile.Server.Domain
	} else {
		url = "http://" + configFile.Server.Domain + ":" + strconv.Itoa(configFile.Server.Port)
	}
	jetStr, vsStr, valid := color.WhiteString("[Jetbrains]"), color.WhiteString("[Vscode]"), color.WhiteString("[Valid tokens]")
	fmt.Printf("%s: %s/copilot_internal/v2/token\n%s: %s\n%s: %d\n",
		jetStr, color.HiBlueString(url), vsStr, color.HiBlueString(url), valid, len(validGhuTokenMap))
	fmt.Println("-----------------------------------------------------------------------")
	for {
		requestCountMutex.Lock()
		sCount := successCount
		tCount := requestCount
		gCount := githubApiCount
		requestCountMutex.Unlock()
		currentTime := time.Now().Format("2006-01-02 15:04:05")
		if "00:00:00" == currentTime {
			resetRequestCount()
		}
		s2, s3, s4 := color.WhiteString("[Succeed]"), color.WhiteString("[Failed]"), color.WhiteString("[GithubApi]")
		// 打印文本
		fmt.Printf("\033[G%s  -  %s: %s    %s: %s    %s: %s  ",
			color.HiYellowString(currentTime),
			s2, color.GreenString(strconv.Itoa(sCount)),
			s3, color.RedString(strconv.Itoa(tCount-sCount)),
			s4, color.CyanString(strconv.Itoa(gCount)))
		time.Sleep(1 * time.Second) //
	}
}
