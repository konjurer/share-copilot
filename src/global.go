package main

import (
	"github.com/gin-gonic/gin"
	"sync"
)

// Config 配置文件结构体
type Config struct {
	Server struct {
		Domain   string `json:"domain"`
		Host     string `json:"host"`
		Port     int    `json:"port"`
		CertPath string `json:"certPath"`
		KeyPath  string `json:"keyPath"`
	} `json:"server"`
	CopilotConfig struct {
		GithubApiUrl string   `json:"github_api_url"`
		Token        []string `json:"token"`
	} `json:"copilot_config"`
	Verification string `json:"verification"`
	DiyMsg       string `json:"diyMsg"`
	IsModMsg     bool   `json:"isModMsg"`
}

// 全局变量
var (
	copilotGinEngine *gin.Engine
	//有效ghu_token的map
	validGhuTokenMap = make(map[string]bool)
	//与有效ghu_token对于的co_token的map
	copilotTokenMap = make(map[string]map[string]interface{})
	//服务器配置文件
	configFile Config
	//请求计数锁
	requestCountMutex sync.Mutex
	//githubApi请求计数
	githubApiCount = 0
	//总请求计数
	requestCount = 0
	//请求成功计数
	successCount = 0

	completionUrl = "https://copilot-proxy.githubusercontent.com/v1/engines/copilot-codex/completions"
	telemetryUrl  = "https://copilot-telemetry.githubusercontent.com/telemetry"
)
