package main

func init() {
	loadConfig()           // 1.加载服务器配置文件
	initGinEngine()        // 2.初始化Gin引擎
	initValidGhuTokenMap() // 3.初始化有效Ghu_token
}

func main() {
	Routes()      // 1.url路由
	StartServer() // 2.启动服务器
	showMsg()     // 3.控制台信息显示
}
